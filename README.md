# PlayListSaver

### What is this repository for? ###

* This repository hosts code for the automation of saving a playlist
based on listening history of a given time
* v1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Only 1 Environment variable set
    * `PYTHONUNBUFFERED=1`
* Dependencies :-
    * `Python 3.6.3`
    * `pip install -r requirements.txt`
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Before a merge DO TAKE owner consent.
* Other guidelines

### Who do I talk to? ###

* Mayank Pathak <mayankpathaksmailbox@rediffmail.com>